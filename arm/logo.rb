red			= "\033[1;31m"
blue 		= "\033[1;33m"
green		= "\033[1;32m"
yellow		= "\033[1;34m"

puts """
#{red}██████╗ ███████╗██████╗ #{blue}███████╗██╗   ██╗███████╗                                        
#{red}██╔══██╗██╔════╝██╔══██╗#{green}██╔════╝╚██╗ ██╔╝██╔════╝                                        
#{red}██████╔╝█████╗  ██║  ██║#{red}█████╗   ╚████╔╝ █████╗                                          
#{red}██╔══██╗██╔══╝  ██║  ██║#{yellow}██╔══╝    ╚██╔╝  ██╔══╝                                          
#{red}██║  ██║███████╗██████╔╝#{blue}███████╗   ██║   ███████╗                                        
#{red}╚═╝  ╚═╝╚══════╝╚═════╝ #{green}╚══════╝   ╚═╝   ╚══════╝                                        
                                                                                         
                       #{green} ██╗     ██╗██╗      ██████╗ ██╗   ██╗██╗  ██╗█████#{blue}██╗            
                        #{blue}██║     ██║██║      ██╔══██╗██║   ██║██║ ██╔╝██╔════╝            
            #{blue}█████╗█████╗██║     ██║██║█████╗#{red}█#{blue}█║  ██║██║   ██║█████╔╝ █████╗█#{yellow}████╗█████╗  
            #{blue}╚════╝╚════╝██║     ██║██║╚════╝██║  ██║██║   ██║██╔═██╗ ██╔══╝╚════╝╚════╝  
                        #{red}███████╗██║███████╗ ██████╔╝╚██████╔╝██║  ██╗████#{red}███╗            
                        #{red}╚══════╝╚═╝╚══════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚══════╝            
                                                                                         
█#{blue}█████╗ ██████╗  ██████╗ ██████╗ ██╗   ██╗ ████#{blue}██╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██║   ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
██████╔╝██████╔╝██║   ██║██║  ██║██║   ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
██╔═#{red}══╝ #{yellow}██╔══██╗██║   ██║██║  ██║██║   █#{red}█║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║     ██║  ██║╚██████╔╝██████╔╝╚██████╔╝╚██#{blue}████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═════╝  ╚═════╝  ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝\033[1;37m
"""