system("clear") or system("cls")
system("title RedEye 1.1")
# well.
#  if user os is Linux, then system will 'clear'
# else.
#  if user os is Windows, then system will 'cls'
# end. :)

require 'socket'
require_relative 'lib/port_scanner'
require_relative 'lib/admin_login_finder'
require_relative 'lib/server_scanner'

system("ruby arm/logo.rb")		# im using system
								# to call the logo

# 1.0
#  -port scanner
#  -Admin Login Finder
#  -server scanner


while true
	printf("wsav.redeye@root~#   ")
	x = gets.chomp
	if x=='?'
		puts """
?                               you look at me baby
use_port_scanning               hanya melakukan port scanning saja
use_admin_login_finder          melakukan admin login finder  <membutuhkan url>
use_server_scanner              melakukan server scanner

		"""
	elsif x=='use_admin_login_finder'
		printf("Target Url              : ")
		url = gets.chomp
		admin_login_finder_def(url)
	elsif x=='use_server_scanner'
		printf("Target Url              : ")
		url = gets.chomp
		server_scanner_def(url)
	elsif x=='use_port_scanning'
		printf("target host             : ")
		host = gets.chomp
		threads = []
		port_lis = [9,20,21,22,23,25,37,42,43,49,67,68,69,79,80,88,109,110,111,218,209,427,491,513,561,660,694,752,754,989,990,1241,1311,3306,4001,5000,5050,5985,1443,443,53,1443,135]
		port_lis.each { |i| 
			threads << Thread.new {
				scans(host,i)
			}
		}
		threads.each(&:join)
	else 
		puts "invalid!\nNOTE: Gunakan '?' untuk melihat tutorial"
	end
end